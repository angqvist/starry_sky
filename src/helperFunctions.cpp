#include "helperFunctions.hpp"
#include <numeric>
#include <functional>
#include <cmath>
#include <iostream>
#include <omp.h>
std::vector<std::vector<double>> getForces(const std::vector<Star> &stars)
{
    std::vector<std::vector<double>> forces(stars.size(), std::vector<double>(2, 0));
    for (int i = 0; i < stars.size(); i++)
    {
        #pragma omp parallel
        for (int j = i + 1; j < stars.size(); j++)
        {
            auto force = calculate_force(stars[i], stars[j]);
            for (int k = 0; k < 2; k++)
            {
        #pragma omp critical
                {
                    forces[i][k] -= force[k];
                    forces[j][k] += force[k];
                }
            }
        }
    }
    return forces;
}

std::vector<double> calculate_force(const Star &star1, const Star &star2)
{
    // AU units
    const double G = 4.3e-3;
    auto distance_diff = star1.getPosition();
    distance_diff[0] -= star2.getPosition()[0];
    distance_diff[1] -= star2.getPosition()[1];
    double distance_square = pow(distance_diff[0], 2) + pow(distance_diff[1], 2);
    double distance = sqrt(distance_square);

    std::vector<double> force(2);
    for (int i = 0; i < 2; i++)
    {
        force[i] = (distance_diff[i] / distance) * G * star1.getMass() * star2.getMass() / distance_square;
    }
    return force;
}
#pragma once
#include <vector>
#include "Star.hpp"

std::vector<std::vector<double>> getForces(const std::vector<Star> &stars);
std::vector<double> calculate_force(const Star &, const Star &);

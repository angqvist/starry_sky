#pragma once
#include <vector>

class Star
{
  public:
    Star(const double, const std::vector<double> &, const std::vector<double> &);
    void updatePosition(const std::vector<double> &, const double);
    void updateVelocity(const std::vector<double> &, const double);

    /// Getter and setter for position
    std::vector<double> getPosition() const
    {
        return position;
    }
    void setPosition(const std::vector<double> newPosition)
    {
        position = newPosition;
    }

    /// Getter and setter for velocity
    std::vector<double> getVelocity() const
    {
        return velocity;
    }
    void setVelocity(const std::vector<double> newVelocity)
    {
        velocity = newVelocity;
    }
    double getMass() const
    {
        return mass;
    }

  private:
    double mass;
    std::vector<double> position;
    std::vector<double> velocity;
    std::vector<double> intermediateVelocity;
};

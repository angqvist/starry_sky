#include "Star.hpp"

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/eigen.h>
#include "helperFunctions.hpp"

namespace py = pybind11;

PYBIND11_MODULE(_starry_sky, m)
{
    // py::module m("_starry_sky", "pybind11 _starry_sky plugin");

    // Disable the automatically generated signatures that prepend the
    // docstrings by default.
    py::options options;
    options.disable_function_signatures();

    py::class_<Star>(m, "Star")
        .def(py::init<const double, const std::vector<double> &, const std::vector<double> &>())
        .def("update_position", &Star::updatePosition)
        .def("update_velocity", &Star::updateVelocity)
        .def_property("velocity", &Star::getVelocity, &Star::setVelocity)
        .def_property("position", [](const Star &star) {
            auto position = star.getPosition();
            return py::array(position.size(), position.data());
        }, &Star::setPosition)
        .def_property_readonly("mass", &Star::getMass);

    m.def("calculate_forces", &getForces);
}  

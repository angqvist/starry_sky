#include "Star.hpp"

Star::Star(const double newMass, const std::vector<double> &newPosition, const std::vector<double> &newVelocity)
{
    mass = newMass;
    position = newPosition;
    velocity = newVelocity;
    intermediateVelocity.resize(2);
}

void Star::updatePosition(const std::vector<double> &force, const double timeStep)
{
    std::vector<double> acceleration(2);
    acceleration[0] = force[0] / mass;
    acceleration[1] = force[1] / mass;

    intermediateVelocity[0] = velocity[0] + 0.5 * acceleration[0] * timeStep;
    intermediateVelocity[1] = velocity[1] + 0.5 * acceleration[1] * timeStep;
    position[0] += intermediateVelocity[0] * timeStep;
    position[1] += intermediateVelocity[1] * timeStep;
}

void Star::updateVelocity(const std::vector<double> &force, const double timeStep)
{
    std::vector<double> acceleration(2);
    acceleration[0] = force[0] / mass;
    acceleration[1] = force[1] / mass;

    velocity[0] = intermediateVelocity[0] + 0.5 * acceleration[0] * timeStep;
    velocity[1] = intermediateVelocity[1] + 0.5 * acceleration[1] * timeStep;
}
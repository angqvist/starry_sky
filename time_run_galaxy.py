from starry_sky.galaxy import Galaxy

import time


for n_stars in [50, 100, 500, 2000]:
    galaxy = Galaxy(number_of_stars=n_stars)
    t = time.process_time()
    galaxy.run(30, 86400.0)
    total_time = time.process_time() - t
    print(n_stars, total_time)

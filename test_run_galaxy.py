from starry_sky.galaxy import Galaxy
from starry_sky import mass_weight, dist_scale

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation



galaxy = Galaxy(number_of_stars=50)

fig = plt.figure(figsize=(15, 15))
ax = fig.add_axes([0, 0, 1, 1])
ax.grid(True)

view_scale = 5
ax.set_xlim(-view_scale*dist_scale, view_scale*dist_scale)
ax.set_ylim(-view_scale*dist_scale, view_scale*dist_scale)

frame = galaxy.frame
masses = [2*star.mass/mass_weight for star in galaxy.stars]
scat = ax.scatter([0]*len(galaxy.stars), [0]*len(galaxy.stars))


def propagate_galaxy(i):    
    galaxy.run(25, 15000.0)
    frame = galaxy.frame        
    scat.set_offsets(frame)
    return galaxy.frame

ani = FuncAnimation(fig, propagate_galaxy, interval=1)
plt.show()
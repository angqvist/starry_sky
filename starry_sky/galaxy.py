

import numpy as np
import random
# from starry_sky.star import Star
from _starry_sky import Star

from _starry_sky import calculate_forces

from starry_sky import mass_weight, dist_scale


class Galaxy(object):
    """
    Galaxy object.
    """

    def __init__(self, number_of_stars=100, name='Galaxy', center=[0, 0]):

        self._stars = self.generate_random_stars(number_of_stars)
        self.name = name
        self.center = self.center_of_mass

    def generate_random_stars(self, number_of_stars):
        """
        Generate stars with random position.
        """
        stars = []
        for _ in range(number_of_stars):
            x_pos, y_pos = random.uniform(-dist_scale,
                                          dist_scale), random.uniform(-dist_scale,
                                                      dist_scale)
            mass = random.uniform(0.5*mass_weight, 10*mass_weight)
            velocity_scale = 1e-7/mass      
            velocity = np.array(
                [dist_scale*random.random()*-velocity_scale, dist_scale*random.random()*velocity_scale])
            
            star = Star(mass, np.array([x_pos, y_pos]), velocity)
            stars.append(star)
        return stars

    @property
    def stars(self):
        return self._stars

    def run(self, steps, time_step):
        """
        Run the galaxy for number of steps.
        """

        self.time_step = time_step
        for i in range(steps):
            self.propagate()

    def propagate(self):
        """
        Propagate the stars
        """
        forces = self._get_forces()
        assert len(forces) == len(self.stars)
        # print(forces[0])
        for star, force in zip(self.stars, forces):
            star.update_position(force, self.time_step)

        new_forces = self._get_forces()
        for star, force in zip(self.stars, new_forces):
            star.update_velocity(force, self.time_step)

    def _get_forces(self):
        """
        Get the force of each star in the galaxy.
        """
        return calculate_forces(self.stars)
        forces = [np.array([0.0, 0.0]) for _ in range(len(self.stars))]
        # print(forces)
        for i, star_i in enumerate(self.stars):
            for j, star_j in enumerate(self.stars):
                if j <= i:
                    continue
                force_i_j = self._calculate_force(star_i, star_j)
                forces[i] -= force_i_j
                forces[j] += force_i_j
        return forces

    @property
    def center_of_mass(self):
        """ Position of center of mass."""
        total_mass = 0.0
        center_of_mass = np.array([0.0, 0.0])
        for star in self.stars:
            total_mass += star.mass
            center_of_mass += star.mass * star.position

        return center_of_mass / total_mass

    @property
    def frame(self):
        """ list of [x pos ,y pos, mass] of stars in the galaxy. """

        x_pos = []
        y_pos = []
        for star in self.stars:
            x_pos.append( star.position[0] )
            y_pos.append( star.position[1] )

        return [x_pos, y_pos]

    def _calculate_force(self, star1, star2):
        """
        Calculates the force between star1 and star2

        f = G * m1 * m2 /r^2
        """
        G = 6.674e-3

        distance_diff = star1.position - star2.position
        dist_square = np.linalg.norm(distance_diff)**2
        force_direction = distance_diff/np.sqrt(dist_square)
        force = G * star1.mass*star2.mass/dist_square * force_direction
        return force

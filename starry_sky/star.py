
class Star(object):
    def __init__(self, mass, position, velocity):
        self.mass = mass
        self.position = position
        self.velocity = velocity


    
    def update_position(self, force, timestep):
        """
        Updates the position according to external force.

        force : 3, NumPy array
        """
        acceleration = force / self.mass
        self._intermediate_velocity = self.velocity + 0.5 * acceleration * timestep
        self.position += self._intermediate_velocity * timestep
    
    def update_velocity(self, force, timestep):
        """
        Update the velocity using velocty verlet algorithm
        """

        acceleration = force / self.mass
        self.velocity = self._intermediate_velocity + 0.5 * acceleration * timestep
    
        


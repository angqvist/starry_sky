import matplotlib.pyplot as plt
import numpy as np
import matplotlib.animation as animation




def plot_star_frames(frames):
    numframes = len(frames)
    numpoints = len(frames[0])
    fig = plt.figure()
    scat = plt.scatter(frames[0], s=100)

    ani = animation.FuncAnimation(fig, update_starry_plot, frames=range(numframes),
                                  fargs=(frames, scat))




def update_starry_plot(i, frames, scat):
    """
    Update the starry plot

    parameters
    ---------
    i : frame index
    frames : all the frames
    scat : the scatter plot handle
    """
    scat.set_array(frames[i])

def main():
    numframes = 100
    numpoints = 10
    color_data = np.random.random((numframes, numpoints))
    x, y, c = np.random.random((3, numpoints))

    fig = plt.figure()
    scat = plt.scatter(x, y, c=c, s=100)

    ani = animation.FuncAnimation(fig, update_plot, frames=range(numframes),
                                  fargs=(color_data, scat))
    plt.show()

def update_plot(i, data, scat):
    scat.set_array(data[i])
    return scat,





main()